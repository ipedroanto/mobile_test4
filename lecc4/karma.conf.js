// Karma configuration
// Generated on Sun Jan 10 2021 23:16:31 GMT-0300 (hora estándar de Argentina)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'src/**/*.spec.js'
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'junit'],
    junitReporter: {
      outputDir: process.env.JUNIT_REPORT_PATH || '', // para especificar el nombre de salida de la carpeta donde se guardaran los archivos.
      outputFile: process.env.JUNIT_REPORT_NAME || undefined, // para especificar el nombre de salida del archivo de reportes.
      suite: '', // para especificar un nombre de paquete para la suite.
      useBrowserName: false, // para agregar reportes de navegador.
      nameFormatter: undefined, // function (browser, result) para personalizar el nombre de los atributos.
      classNameFormatter: undefined, // function (browser, result) para personalizar el nombre de las clases.
      properties: {}
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
