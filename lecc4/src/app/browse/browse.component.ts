import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as SocialShare from "@nativescript/social-share";
import { ImageSource } from "@nativescript/core";



@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    public shareText() {
        SocialShare.shareText("I love NativeScript!", "How would you like to share this text?");
    }

    public shareImage() {
        ImageSource.fromUrl("https://www.cameraegg.org/wp-content/uploads/2013/02/Leica-M-Sample-Image.jpg").then((image) => {
            SocialShare.shareImage(image);
        });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
