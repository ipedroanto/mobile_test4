import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ImageSource } from "@nativescript/core";
// import { camera} from "@nativescript/camera";
import { isAvailable, requestCameraPermissions, takePicture } from '@nativescript/camera';
import * as SocialShare from "@nativescript/social-share";
@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
    onImageTap(): void {
        requestCameraPermissions().then(
            function success() {
                const options = {width: 300, height: 300, keepAspectRatio: false, saveToGallery: false};
                console.log(isAvailable());
                takePicture(options)
                    .then((imageAsset) => {
                        ImageSource.fromAsset(imageAsset)
                            .then((imageSource) => {
                                SocialShare.shareImage(imageSource, "Imagen de prueba");
                                this.imagePath = imageSource;
                            });
                    }).catch((err) => {
                    console.log("Error -> " + err.message);
                });
            },
            function failure() {
                console.log("error!!!");
            }
        );
    }
}
