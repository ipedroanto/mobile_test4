
import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { registerElement } from "@nativescript/angular";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
const gmaps = require("nativescript-google-maps-sdk");

@Component({
    selector: "Maps",
    templateUrl: "./maps.component.html"
})
export class MapsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();

    }

    onMapReady($event: any) {
        console.log("Map Ready");
        const mapView = $event.object;
        mapView.latitude = -34.6037;
        mapView.longitude = -58.3817;
        mapView.zoom = 8;
        const marker = new gmaps.Marker();
        marker.position = gmaps.Position.positionFromLatLng(-34.6037, -58.3817);
        marker.title = "Buenos Aires";
        marker.snippet = "Argentina";
        marker.userData = {index: 1};
        mapView.addMarker(marker);
    }
}
