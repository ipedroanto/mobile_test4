import { Component, EventEmitter, Input, Output} from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { ngModel } from '@angular/core';
// import { textAlignmentProperty } from '@nativescript/core';

@Component({
  selector: 'SearchForm',
  moduleId: module.id,
  // templateUrl: "./search-form.component.html",
  template:`
  <TextField #texto="ngModel" [(ngModel)]="textFieldValue"hint="Ingresar texto..." required minlen="4">
  </TextField>
  <Label *ngIf="texto.hasError('required')" text="*Cero caracteres*"></Label>
  <Label *ngIf="!texto.hasError('required')&& texto.hasError('minlen')" text="mínimo 4+"></Label>
  <Button text="Buscar!" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>`
       
  
})
export class SearchFormComponent {
  textFieldValue: string = "";
  @Output() search: EventEmitter<string> = new EventEmitter();
  @Input() inicial: string;

  ngOnInit(): void {
    this.textFieldValue = this.inicial;
  }
  onButtonTap(): void {
    console.log(this.textFieldValue);
    if(this.textFieldValue.length >2){
      this.search.emit(this.textFieldValue);
    }
  }

}