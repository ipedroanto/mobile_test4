import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as Toast from "nativescript-toast";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, ApplicationSettings, Dialogs, Label, View } from "@nativescript/core";
import { DURATION, show, ToastOptions } from "nativescript-toasts";
import { Router } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";

@Component({
    selector: "Settings",
    moduleId: module.id,
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {
    @ViewChild("lblNombreDeUsuario") lblNombreDeUsuario: ElementRef;
    _nombreUsuario: string;
    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    doLater(fn){ setTimeout(fn, 500); }

    public get nombreUsuario(): string{
        if( ApplicationSettings.hasKey("nombreUsuario") && ApplicationSettings.getString("nombreUsuario") != "") {
            //console.log("Nombre de usuario guardado: " + ApplicationSettings.getString("nombreUsuario"));
            this._nombreUsuario = "Nombre de usuario: " + ApplicationSettings.getString("nombreUsuario");
        }
        else{
            this._nombreUsuario = "Nombre de usuario: --";
            const toastsOptions: ToastOptions = {text: "Ingrese su nombre de usuario", duration: DURATION.LONG};
            this.doLater(() => show(toastsOptions));
        }
        return this._nombreUsuario;
    }

    ngOnInit(): void {
        /*
        if( ApplicationSettings.hasKey("nombreUsuario") ) {
            console.log(ApplicationSettings.getString("nombreUsuario"));
            const lblNombreDeUsuario = <Label>this.lblNombreDeUsuario.nativeElement;
            if(lblNombreDeUsuario != null)
                lblNombreDeUsuario.text = "Nombre de usuario: " + ApplicationSettings.getString("nombreUsuario");
            else
                console.log("No se pudo leer la configuracion");
        }
        else{
            const toastsOptions: ToastOptions = {text: "Ingrese su nombre de usuario", duration: DURATION.LONG};
            this.doLater(() => show(toastsOptions));
        }
        */

        /*
        this.doLater(() => 
            Dialogs.action("Mensaje", "Cancelar!", ["Opcion1", "Opcion2"])
                .then((result) => {
                    console.log("resultado: " + result);
                    if(result === "Opcion1") {
                        this.doLater(() =>
                            Dialogs.alert({
                                title: "Titulo 1",
                                message: "Mensaje 1",
                                okButtonText: "btn 1"
                            }).then(() => console.log("Cerrado 1!")));
                    } else if(result === "Opcion2"){
                        this.doLater(() =>
                            Dialogs.alert({
                                title: "Titulo 2",
                                message: "Mensaje 2",
                                okButtonText: "btn 2"
                            }).then(() => console.log("Cerrado 2!")));
                    }
                }
        ));
        */
       Toast.makeText("Hello World").show();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.closeDrawer();
    }
}
